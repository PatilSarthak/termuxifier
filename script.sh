#!/bin/bash


fontsrc=./fonts
fontdes=~/.termux/font.ttf
refresh=termux-reload-settings
randomColor=$(ls colors | sed -n $(($RANDOM/200))p)


required(){

echo "updating repo and installing missing dependencies"
pkg update -y && pkg upgrade -y
pkg install -y zsh wget figlet git

echo "cloning kdrag0n's base16-termux repo"
git clone https://github.com/kdrag0n/base16-termux ./colors && rm -r ./colors/templates ./colors/LIC* ./colors/README.md && mv ./colors/colors/*.* ./colors/ && rm -r ./colors/colors

clear && start
}

font(){

# https://www.reddit.com/r/i3wm/comments/33xsp9/suggest_me_a_font/

clear
echo "which font do you want make as a default font"
echo "[1] fantasque-sans-mono"
echo "[2] ububtu" 
echo "[3] SourceSansPro" 
echo "[4] firacode"
echo "[5] lemon"
echo "[9] Exit" 
echo "[0] Go back"
read fontnum
case $fontnum in
1) cp ./fonts/FantasqueSansMono.ttf ~/.termux/font.ttf && $refresh || echo "font is missing, update the repo" && echo "font has been set as a FantasqueSansMono, Restart termux to change effects" ;;

2) cp ./fonts/ubuntu.ttf ~/.termux/font.ttf && $refresh || echo "font is missing, update repo" && echo "font has been set as ubuntu, Restart termux to change effetcs" ;;

3) cp ./fonts/SourceSansPro.ttf ~/.termux/font.ttf && $refresh ;;

4) cp $fontsrc/firacode.ttf $fontdes && $refresh;;

5) cp $fontsrc/lemon.ttf $fontdes && $refresh;;

9) clear && exit ;;

0) clear && start

esac
clear && font
}



plugins(){
#https://github.com/ohmyzsh/ohmyzsh
#https://github.com/zsh-users


echo "installing zsh..."
sh -c "$(wget -O- https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
chsh -s $(which zsh)
echo "adding zsh-syntax-highlighting in "
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ~/.oh-my-zsh/plugins/zsh-syntax-highlighting
echo "adding zsh-autosuggestions plugin in "
git clone https://github.com/zsh-users/zsh-autosuggestions ~/.oh-my-zsh/plugins/zsh-autosuggestions

clear && start
}


color(){
# https://github.com/kdrag0n/base16-termux
clear
randomColor=$(ls colors | sed -n $(($RANDOM/200))p)
echo "[1] random"
echo "[2] select"
echo "[3] Go back"

read colorNo
case $colorNo in
1) echo "Selected $randomColor" && cp -f colors/$randomColor ~/.termux/colors.properties && $refresh ;;

2) ls ./colors/ && total=$(ls ./colors/ | wc -l) && echo "total themes are $total.Type a number to select theme: "  && read luckyNum && test=$(ls colors | sed -n $(($luckyNum))p) && echo $test && cp -f ./colors/$test ~/.termux/colors.properties && $refresh ;;

3) clear && start ;;

esac
clear && color
}

#######  next time
#touch ~/.hushlogin
#termux-setup-storage

start(){

echo "[1] change font"
echo "[2] change color scheme"
echo "[3] Install zsh & plugins"
echo "[4] Dependencies & required tools (imp)"
echo "[9] Exit"

echo "type a number:"
read num
case $num in 
1) font ;;
2) color ;;
3) plugins ;;
4) required ;;
9) clear && exit ;;
esac
}
clear
start
